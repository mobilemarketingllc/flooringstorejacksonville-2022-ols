<?php
// Variable for Product Loop listing page Column
$col_css = postpercol + 1;

$color = (get_option('api_colorcode_sandbox') != "")?get_option('api_colorcode_sandbox'):"#00247a";
$seleinformation = json_decode(get_option('saleinformation'));
$seleinformation = (array)$seleinformation;

$salespriority = json_decode(get_option('salespriority'));
$salespriority = (array)$salespriority;
$high_priprity = min($salespriority);


if(isset($_GET['promo']) && !empty($_GET['promo'])) {

    $promo_code = $_GET['promo'];

}else{ 

    $promo_code = array_search($high_priprity,$salespriority);
}
//echo $promo_code;
//echo '<pre>';
//print_r($seleinformation[$promo_code]->sale_name);

$brand_cls = "";
$product_json =  json_decode(get_option('product_json'));
    
    if(is_array($product_json) && count($product_json) > 0){
       
            foreach($product_json as $product_detail){
                if(isset($product_detail->manufacturer)){

                    $brand_cls .=  ".".strtolower(str_replace(" ","-",$product_detail->manufacturer)).","; 
                    // $check_brand_lp = get_page_by_title(trim($product_detail->manufacturer) );
                    // if(isset($check_brand_lp) && get_post_status($check_brand_lp->ID) == "publish"){
                    //     $brand_cls .=  ".".$check_brand_lp->post_name.","; 
                    // }
                }
            }
    }
    $brand_cls = rtrim($brand_cls,",");

// kitchen and windows treatment

    $website_json =  json_decode(get_option('website_json'));

    $website_options = $website_json->options->kitchenBath;

    $windowstyle = '';

    if($website_json->options->windowTreatment == '1'){

       $windowstyle .= '.window_section,';

    }

    if($website_json->options->kitchenBath == '1'){

        $windowstyle .= '.kitchen_section,';

        if( $website_json->options->kitchenBathCountertop == '1' ){

            $windowstyle .= '.countertops_section,';
    
        }
        if( $website_json->options->kitchenBathCabinet == '1' ){
    
         
            $windowstyle .= '.cabinets_section,';
            
        }
        if( $website_json->options->kitchenBathVanity == '1' ){
    
    
            $windowstyle .= '.vanities_section,';
        }
        if( $website_json->options->kitchenBathHardware == '1' ){
    
            $windowstyle .= '.hardware_section,';
            
        }

    } 

    $productLandings = json_decode(json_encode($website_json->options),true);

    if(is_array($productLandings) && count($productLandings) > 0){
        foreach($productLandings as $cat => $status){
            if($status == '1'){
                $windowstyle .= '.'.trim($cat).',';
            }
        }
    }


    $windowstyle = rtrim($windowstyle,',');

?>
<style>

<?php 
if($website_json->options->kitchenBath == '0' || $website_json->options->windowTreatment == '0' || $website_json->options->kitchenBathCountertop == '0'){ ?>

    .home_kitchen_window{
        display:none;
    }

<?php } ?>

    .brand_section_hide, .hide-sec{
        display:none !important;
    }
    .product_category_block .hide-sec{
        padding:10px;
    }
    <?php echo $brand_cls ?>{
        display:block !important;
    }
    <?php echo $windowstyle ?>{
        display:block !important;
    }
.salebanner-slide{
	max-height:440px!important;
	margin: 0 auto;
    display: block;
}
.homepage-banner-bg{
	background-image: url(<?php if ( isset( $seleinformation[$promo_code]->background_image )){echo $seleinformation[$promo_code]->background_image;}?>)!important;
	background-repeat: no-repeat;
}
.fl-row-content-wrap.nosalebg {    
	background-image: url(<?php echo "https://mmllc-images.s3.us-east-2.amazonaws.com/beautifall-financing/no-promo-background.jpg";?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}
.fl-row-content-wrap.custombg {    
	background-image: url(<?php  if ( isset( $seleinformation[$promo_code]->background_image_landing )){echo $seleinformation[$promo_code]->background_image_landing;}?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}
.fl-row-content-wrap.homepage {
    background-image: url(<?php if ( isset( $seleinformation[$promo_code]->background_image )){echo $seleinformation[$promo_code]->background_image;}?>)!important;
//	background-image: url(<?php if ( isset( $seleinformation[$promo_code]->background_image_landing )){echo $seleinformation[$promo_code]->background_image_landing;}?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}

img.salebanner.fl-slide-photo-img {
    max-width: 500px;
}

.banner-mobile{
	display: none;
	text-align:center;
}

@media(max-width:1080px){
.fl-page-header-primary .fl-logo-img{
max-width: 80%;
}
}

@media(max-width:992px){
.fl-page-header-primary .fl-logo-img{
max-width: 50%;
}
.banner-below-heading-n-text h2 span{
font-size: 25px;
line-height: 28px;
}
.banner-deskop{
display: none;
}
.banner-mobile{
	display: block;
}
}
</style>
